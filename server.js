var express = require('express');
var app = express();
var port = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var nodemailer = require('nodemailer');
app.use(morgan('dev')); 
app.use(cookieParser()); 
app.use(bodyParser()); 
app.set('view engine', 'jade'); 
app.use(session({
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true },
    secret: 'secretPass'
})); 
app.use(passport.initialize());
app.use(passport.session()); 
app.use(flash());
mongoose.connect("mongodb://localhost/userdetail", function (err) {
    if (err) {
        console.log("DB Error: ", err);
        process.exit(1);
    } else {
        console.log('MongoDB Connected');
    }
});
require('./routes/routes')(app, passport);
require('./routes/products')(app);
require('./routes/cart')(app);
require('./config/passport')(passport);
app.listen(3000, () => console.log('Example app listening on port 3000!'));