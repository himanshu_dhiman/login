var Controller = require('../controllers');
var Joi = require('joi');

module.exports = function (app) {

    app.route('/addToCart').post(function (req, res) {
        const validator = {
            userId: Joi.string().required(),
            id: Joi.string().required(),
            quantity: Joi.number().required(),
        }
        const bodyValidation = Joi.validate(req.body, validator);
        if (bodyValidation.error) {
            res.status(400).send({
                "Message": bodyValidation.error.message
            });
            return;
        }
        Controller.cartController.addToCart(req.body, function (err, data) {
            if (err) {
                res.status(400).send({
                    "Message": err,
                })
                return;
            } else {
                res.status(200).send({
                    "Message": "Success",
                    "data": data
                })
            }
        })

    })
    app.route('/removeFromCart').delete(function (req, res) {
        const validator = {
            userId: Joi.string().required(),
            id: Joi.string().required(),
        }
        const bodyValidation = Joi.validate(req.body, validator);
        if (bodyValidation.error) {
            res.status(400).send({
                "Message": bodyValidation.error.message
            });
            return;
        }
        Controller.cartController.removeFromCart(req.body, function (err, data) {
            if (err) {
                res.status(400).send({
                    "Message": err,
                })
                return;
            } else {
                if (data.n) {
                    if (data.nModified == "1") {
                        res.status(200).send({
                            "Message": "Successfully Deleted",
                        })
                    } else {
                        res.status(200).send({
                            "Message": "NO orders with this orderId",
                        })
                    }
                } else {
                    res.status(200).send({
                        "Message": "NO orders with this userId",

                    })
                }
            }
        })

    })
    app.route('/viewCart').post(function (req, res) {
        const validator = {
            userId: Joi.string().required(),
        }
        const bodyValidation = Joi.validate(req.body, validator);
        if (bodyValidation.error) {
            res.status(400).send({
                "Message": bodyValidation.error.message
            });
            return;
        }
        Controller.cartController.fetchCart(req.body, function (err, data) {
            if (err) {
                res.status(400).send({
                    "Message": err,
                })
                return;
            } else {
                if (data.length) {
                    res.status(200).send({
                        "Message": "Success",
                        "data": data
                    })
                } else {
                    res.status(200).send({
                        "Message": "NO orders with this userId",

                    })
                }
            }
        })

    })
}