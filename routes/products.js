var Controller = require('../controllers');
var Joi = require('joi');
module.exports = function (app) {
    app.route('/saveProductDetails').post(function (req, res) {
        const validator = {
            category: Joi.string().required(),
            productName: Joi.string().required(),
            productType: Joi.string().required(),
            brandName: Joi.string().required(),
            size: Joi.string().required(),
            colour: Joi.string().required(),
            description: Joi.string().required(),
            weight: Joi.string().required(),
            price: Joi.string().required(),
            discount: Joi.string().required(),
            image: Joi.string().required(),
            features: Joi.string().required(),
            technicalSpecification: Joi.string().required(),
        };
        const bodyValidation = Joi.validate(req.body, validator);
        if (bodyValidation.error) {
            console.log("inside validation");
            res.status(400).send({
                "Message": bodyValidation.error.message
            });
            return;
        }
        Controller.productController.saveProductDetails(req.body, function (err, data) {
            if (err) {
                res.status(400).send({
                    "Message": err,
                })
                return;
            } else {
                res.status(200).send({
                    "Message": "Success",
                    "data": data
                })
            }
        });
    })
    app.route('/fetchOneProduct').post(function (req, res) {
        const validator = {
            id: Joi.string().required(),

        }
        const bodyValidation = Joi.validate(req.body, validator);
        if (bodyValidation.error) {
            res.status(400).send({
                "Message": bodyValidation.error.message
            });
            return;
        }
        Controller.productController.fetchOneProduct(req.body, function (err, data) {
            if (err) {
                res.status(400).send({
                    "Message": err,
                })
                return;
            } else {
                res.status(200).send({
                    "Message": "Success",
                    "data": data
                })
            }
        })

    })
   
    app.route('/editOneProduct').patch(function (req, res) {
        const validator = {
            id: Joi.string().required(),
            category: Joi.string().required(),
            productName: Joi.string().required(),
            productType: Joi.string().required(),
            brandName: Joi.string().required(),
            size: Joi.string().required(),
            colour: Joi.string().required(),
            description: Joi.string().required(),
            weight: Joi.string().required(),
            price: Joi.string().required(),
            discount: Joi.string().required(),
            image: Joi.string().required(),
            features: Joi.string().required(),
            technicalSpecification: Joi.string().required(),
        }
        const bodyValidation = Joi.validate(req.body, validator);
        if (bodyValidation.error) {
            res.status(400).send({
                "Message": bodyValidation.error.message
            });
            return;
        }
        Controller.productController.editOneProduct(req.body, function (err, data) {
            if (err) {
                res.status(400).send({
                    "Message": err,
                })
                return;
            } else {
                res.status(200).send({
                    "Message": "Success",
                    "data": data
                })
            }
        })

    })
    app.route('/deleteOneProduct').delete(function (req, res) {
        const validator = {
            id: Joi.string().required(),
        }
        const bodyValidation = Joi.validate(req.body, validator);
        if (bodyValidation.error) {
            res.status(400).send({
                "Message": bodyValidation.error.message
            });
            return;
        }
        Controller.productController.deleteOneProduct(req.body, function (err, data) {
            if (err) {
                res.status(400).send({
                    "Message": err,
                })
                return;
            } else {
                res.status(200).send({
                    "Message": "Success",
                    "data": data
                })
            }
        })

    })
    app.route('/fetchAllProducts').get(function (req, res) {
        Controller.productController.fetchAllProduct(function (err, data) {
            if (err) {
                res.status(400).send({
                    "Message": err,
                })
                return;
            } else {
                res.status(200).send({
                    "Message": "Success",
                    "data": data
                })
            }
        })

    })
}