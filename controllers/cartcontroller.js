var Service = require('../services');
const addToCart = function (data, callback) {
    var criteria = {
        userId: data.userId
    };
    Service.cartService.fetchCartDetails(criteria, function (err, Data) {
        if (err) {
            return err.message
        } else {
            if (Data.length) {
                var projection = {
                    $push: {
                        cart: {
                            productId: data.id,
                            quantity: data.quantity,
                        }
                    }
                };

                Service.cartService.editCart(criteria, projection, function (err, newData) {
                    console.log(err, newData);
                    if (err) {
                        if (err.code && err.code == 11000) {
                            return callback(err.message);
                        }
                        return callback(err);
                    } else {
                        return callback(null, newData);
                    }
                })

            } else {
                var criteria = {
                    userId: data.userId,
                    cart: {
                        productId: data.id,
                        quantity: data.quantity
                    }
                }
                Service.cartService.addCartDetails(criteria, function (err, newData) {
                    if (err) {
                        if (err.code && err.code == 11000) {
                            return callback(err.message);
                        }
                        return callback(err);
                    } else {
                        return callback(null, newData);
                    }
                })

            }
        }
    })

}
const removeFromCart = function (data, callback) {
    var criteria = {
        userId: data.userId
    };
    Service.cartService.fetchCartDetails(criteria, function (err, Data) {
        if (err) {
            return err.message
        } else {
            console.log(Data, "=======================")
            if (Data.length) {
                var projection = {
                    $pull: {
                        cart: {
                            productId: data.id,
                        }
                    }
                };
                Service.cartService.editCart(criteria, projection, function (err, newData) {
                    console.log(err, newData);
                    if (err) {
                        if (err.code && err.code == 11000) {
                            return callback(err.message);
                        }
                        return callback(err);
                    } else {
                        return callback(null, newData);
                    }
                })

            } else {
                return callback(null, Data);
            }
        }
    })

}
const fetchCart = function (data, callback) {
    var criteria = {
        userId: data.userId
    };
    Service.cartService.fetchCartDetails(criteria, {}, {}, function (err, newData) {
        console.log(err, newData);
        if (err) {
            if (err.code && err.code == 11000) {
                return callback(err.message);
            }
            return callback(err);
        } else {
            return callback(null, newData);
        }
    })
}
module.exports = {
    fetchCart: fetchCart,
    addToCart: addToCart,
    removeFromCart: removeFromCart,
}