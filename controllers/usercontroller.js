const Service = require('../services')
var bcrypt = require('bcryptjs');
const signup = function (data ,callback){
    Service.userService.signup(data,function(err,newData){
            if (err){
                if (err.code && err.code == 11000){
                    return callback(err.message);
                }
                return callback(err);
            }
            else 
            {
              console.log("HERE=============+===============");
              return callback(null, newData);
            }
        })  
     }
     const authenticate =  function(data,callback){
        let criteria = {
            email : data.email
        }
      Service.userService.authenticate(criteria,{},{},function(err,foundUser){
            if(err){
                if (err.code && err.code == 11000){
                    return callback(err.message);
                }
                return callback(err);
            } else if (!foundUser){
                var err = new Error('User not found.');
                 err.status = 401;
                 return callback(err);
            } else{
                bcrypt.compare(data.password, foundUser.password, function (err, result) {
                    if (result === true) {
                        console.log ("=======controller=======");
                      return callback(null, foundUser);
                    } else {
                    // console.log ("PASSWORD MISMATCH !!!!!!");
                    var err = new Error("PASSWORD MISMATCH !!!!!!");
                    err.status = 401;
                    return callback(err);
                    }
               
                }
        )}
    });
}
     module.exports = {
         signup : signup,
         authenticate :authenticate,
     }