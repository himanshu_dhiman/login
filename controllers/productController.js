var Service = require('../services');
const Config = require('../config');
const saveProductDetails = function (data, callback) {
    var criteria = {
        category: data.category,
        productName: data.productName,
        productType: data.productType,
        brandName: data.brandName,
        size: data.size,
        colour: data.colour,
        description: data.description,
        technicalSpecification: data.technicalSpecification,
        weight: data.weight,
        price: data.price,
        discount: data.discount,
        image: data.image,
        features: data.features,
    };
    Service.productService.saveProductDetails(criteria, function (err, newData) {
        if (err) {
            if (err.code && err.code == 11000) {
                return callback(err.message);
            }
            return callback(err);
        } else {
            return callback(null, newData)
        }
    })
}
const fetchAllProduct = function (callback) {
    Service.productService.fetchAllProduct({},{},{},function (err, newData) {
        console.log(err, newData);
        if (err) {
            if (err.code && err.code == 11000) {
                return callback(err.message);
            }
            return callback(err);
        } else {
            return callback(null, newData);
        }
    })
}
const fetchOneProduct = function (data, callback) {
    var projection = {};
    var criteria = {
        _id: data.id
    };
    Service.productService.fetchOneProduct(criteria, projection, function (err, newData) {
        console.log(err, newData);
        if (err) {
            if (err.code && err.code == 11000) {
                return callback(err.message);
            }
            return callback(err);
        } else {
            return callback(null, newData);
        }
    })
}


const editOneProduct = function (data, callback) {
    var projection = {
        category: data.category,
        productName: data.productName,
        productType: data.productType,
        brandName: data.brandName,
        size: data.size,
        colour: data.colour,
        description: data.description,
        technicalSpecification: data.technicalSpecification,
        weight: data.weight,
        price: data.price,
        discount: data.discount,
        image: data.image,
        features: data.features,
    };
    var criteria = {
        _id: data.id
    };
    Service.productService.editProduct(criteria, projection, function (err, newData) {
        console.log(err, newData);
        if (err) {
            if (err.code && err.code == 11000) {
                return callback(err.message);
            }
            return callback(err);
        } else {
            return callback(null, newData);
        }
    })
}
const deleteOneProduct = function (data, callback) {
    var criteria = {
        _id: data.id
    };
    var projection = {};
    Service.productService.removeProduct(criteria, function (err, newData) {
        console.log(err, newData);
        if (err) {
            if (err.code && err.code == 11000) {
                return callback(err.message);
            }
            return callback(err);
        } else {
            return callback(null, newData);
        }
    })
}

module.exports = {
    saveProductDetails: saveProductDetails,
    fetchAllProduct: fetchAllProduct,
    fetchOneProduct: fetchOneProduct,
    editOneProduct: editOneProduct,
    deleteOneProduct: deleteOneProduct,
}