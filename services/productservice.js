var Models = require('../models');
var saveProductDetails = function (objToSave, callback) {
    new Models.products(objToSave).save(callback)
}
var fetchAllProduct = function (criteria, projection, options, callback) {
    Models.products.find(criteria, projection, options, callback);
}
var fetchOneProduct = function (criteria, projection, options, callback) {
    Models.products.findOne(criteria, projection, options, callback);
}
var editProduct = function (query, update, options, callback) {
    Models.products.update(query, update, options, callback);
}
var removeProduct = function(query,projection,callback){
    Models.products.remove(query,projection,callback);
}
module.exports = {
    saveProductDetails: saveProductDetails,
    fetchAllProduct: fetchAllProduct,
    fetchOneProduct: fetchOneProduct,
    editProduct : editProduct,
    removeProduct:removeProduct,
};