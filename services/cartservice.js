var Models = require('../models');
var editCart = function (query, update, options, callback) {
    Models.cart.update(query, update, options, callback);
}
var removeProduct = function (query, projection, callback) {
    Models.cart.update(query, projection, callback);
}
var fetchCartDetails = function (criteria, projection, options, callback) {
    Models.cart.find(criteria, projection, options, callback);
}
var addCartDetails = function (objToSave, callback) {
    new Models.cart(objToSave).save(callback)
}
module.exports = {
    fetchCartDetails: fetchCartDetails,
    addCartDetails:addCartDetails,
    removeProduct: removeProduct,
    editCart: editCart,
}