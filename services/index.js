module.exports = {
    userService: require('./userservice'),
    productService: require('./productservice'),
    cartService: require('./cartservice'),
}