module.exports = {
    userDetails : require('./userdetail'),
    products : require('./products'),
    cart :require('./cart'),
}