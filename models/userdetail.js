var mongoose = require('mongoose');
var Schema = mongoose.Schema ;
var bcrypt = require('bcrypt-nodejs');
var userSchema = new mongoose.Schema({
    username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    resetPasswordToken: String,
    resetPasswordExpires: Date
  });
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

var userObject = mongoose.model('userdetails',userSchema);
module.exports = userObject ;