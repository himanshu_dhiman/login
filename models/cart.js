var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var cartSchema = new mongoose.Schema({
    userId: {
        type: String,
    },
    cart: [{
        productId: {
            type: String,
        },
        quantity: {
            type: Number
        }
    }],
});
var cartObject = mongoose.model('cartdetail', cartSchema);
module.exports = cartObject;