var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userSchema = new mongoose.Schema({
    category: {
        type: String,
    },
    productName: {
        type: String,
    },
    productType: {
        type: String,
    },
    brandName: {
        type: String,
    },
    size: {
        type: String
    },
    colour: {
        type: String,
    },
    description: {
        type: String,
    },
    technicalSpecification: {
        type: String
    },
    weight: {
        type: String
    },
    price: {
        type: String
    },
    discount: {
        type: String
    },
    image: {
        type: String
    },
    features: {
        type: String
    },
    cartStatus: {
        type: String
    },
    cartQuantity: {
        type: String
    },

});


var userObject = mongoose.model('productdetail', userSchema);
module.exports = userObject;